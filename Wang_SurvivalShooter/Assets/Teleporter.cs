﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    public Transform[] teleportSpots;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Transform teleportSpot = teleportSpots[(int)(Random.value * teleportSpots.Length)];
            other.transform.position = teleportSpot.transform.position;
        }
    }
}
