﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class particledestroytimer : MonoBehaviour
{
    public float timeTilDestroy;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("DestroyMe", timeTilDestroy);
    }

    // Update is called once per frame
    void DestroyMe()
    {
        Destroy(gameObject);
    }
}
