﻿using UnityEngine;

public class GameOverManager : MonoBehaviour
{
    public PlayerHealth playerHealth;


    Animator anim;
    float timer;
    public Light EndgameLight;


    void Awake()
    {
        anim = GetComponent<Animator>();
    }


    void Update()
    {
        if (playerHealth.currentHealth <= 0)
        {
            anim.SetTrigger("GameOver");
        }

        timer += Time.deltaTime;

        if (timer >= 90f)
        {
            anim.SetTrigger("GameOver");
            EndgameLight.enabled = true;
        }
    }
}
